import React, { useState } from 'react';
import logo from './logo.svg';
import { randLonLat } from "./utils";
import ReactGlobe from 'react-globe';
import { defaultBarMarkerOptions } from "react-globe";
import './App.css';
import { grommet, Box, Button, Grommet } from "grommet";

// const markers = [
const ogmarkers = [];

function getTooltipContent(marker) {
    return `lon: ${marker.lon} ~ lat: ${marker.lat}`;
}

function tooltipContent(lon, lat) {
    return `lon: ${lon} ~ lat: ${lat}`;
}

function App() {
    const [event, setEvent] = useState(null);
    const [focus, setFocus] = useState(undefined);
    const [details, setDetails] = useState(null);
    const [markers, setMarkers] = useState(ogmarkers);

    function onClickMarker(marker, markerObject, event) {
        setEvent({
            type: 'CLICK',
            marker,
            markerObjectID: markerObject.uuid,
            pointerEventPosition: { x: event.clientX, y: event.clientY },
        });
        setDetails(getTooltipContent(marker));
    }

    function onDefocus(previousCoordinates, event) {
        console.log(previousCoordinates)
        setEvent({
            type: 'DEFOCUS',
            previousCoordinates,
            pointerEventPosition: { x: event.clientX, y: event.clientY },
        });
        setDetails(null);
    }
    // function onDefocus(previousFocus) {
    //     setEvent({
    //       type: "DEFOCUS",
    //       previousFocus
    //     });
    //     setDetails(null);
    //   }


    const thingy = () => {
        // console.log('howdy');
        const { lon, lat } = randLonLat();
        // console.log(lon, lat)
        Number((6.688689).toFixed(1)); // 6.7
        const newplace = {
            id: markers.length + 2,
            color: 'red',
            lon: lon.toFixed(4),

            lat: lat.toFixed(4),
            coordinates: [lat, lon],
            value: 24,
        };
        setMarkers([...markers, newplace])
        setFocus([lat, lon])
        setDetails(tooltipContent(lon, lat));
    };

    return (
        <div style={{ fontFamily: 'arial', width: '100vw', height: '90vh' }}>
            <Box
                border
                justify="center"
                align="center"
                height="10vh"
                width="300px"
            >
                <Button
                    label="Where are you not???"
                    fill
                    radius={2}
                    onClick={() => thingy()}
                />
            </Box>

            <ReactGlobe
                height="100vh"
                markers={markers}
                // options={options}
                focus={focus}
                width="100vw"
                onClickMarker={onClickMarker}
                onDefocus={onDefocus}
                focusOptions={{
                    animationDuration: 1000,
                    distanceRadiusScale: 5,
                    easingFunction: ['Cubic', 'In'],
                    enableDefocus: true,
                }}
            />
            {details && (
                <div
                    style={{
                        background: 'white',
                        position: 'absolute',
                        fontSize: 24,
                        top: 0,
                        right: 0,
                        padding: 12,
                    }}
                >
                    {details}
                </div>
            )}
        </div>
    );
}

export default App;
