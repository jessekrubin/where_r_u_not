
const range = (start=0, stop=10, step = 1) =>
    Array(Math.ceil((stop - start) / step)).fill(start).map((x, y) => x + y * step);


const randFloat= (min=0, max=1) => {
    return Math.abs(max-min)* Math.random() + min;
};

const randLon = () => {
    return randFloat(-180, 180);
};
const randLat = () => {
    return randFloat(-88, 88);
};
export const randLonLat= () => {
    return {lon: randLon(), lat: randLat()};
};
